SHELL=/bin/bash
default: build years
build:
	@[ -d public/splash ] || make screenshots
	@[ -e public/pageimg.png ] || make screenshots
	npm run build
	minify --html-keep-document-tags dist/index.html -o dist/index.html
years:
	y=1; while [ "$$y" -lt 301 ]; do mkdir -p dist/r"$$y"; cp dist/index.html dist/r"$$y"/index.html; perl -pi -e "s{pageimg.png}{splash/y$$y.png}g" dist/"r$$y"/index.html; let y=$$y+1;done
screenshots:
	node build-screenshots.js
rebuild: screenshots build years
