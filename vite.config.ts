import { defineConfig } from "vite";
import { resolve } from "path";
import preact from "@preact/preset-vite";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [preact()],
  appType: "mpa",
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, "index.html"),
        some: resolve(__dirname, "some-bg.html"),
        somePort: resolve(__dirname, "some-bg-portrait.html"),
      },
    },
  },
  resolve: {
    preserveSymlinks: true,
  },
});
