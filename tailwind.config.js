/** @type {import('tailwindcss').Config} */
const designSettings = {
  bakgrunnFarge: "#FAF8F4",
  tekstFarge: "#3C3C3B",
  lenkjeFarge: "#FC1921",
};
const ff_bg = designSettings.bakgrunnFarge;
const ff_text = designSettings.tekstFarge;
const ff_links = designSettings.lenkjeFarge;
const ff_red = "#FC1921";
const ffu_red = "#e51e2a";
const ff_brownbg = "#3A0000";
const ff_pink = "#E28989";
const calc_pink = "#EBC3CD";
const offblack = "#3C3C3B";
module.exports = {
  content: [
    "./*.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "../bolig.fagforbundet.no/src/*.tsx",
  ],
  theme: {
    fontFamily: {
      mareka: ["MAREKA"],
      canela: ["Canela"],
      montserrat: ["Montserrat"],
    },
    container: {
      center: true,
    },
    extend: {
      colors: {
        "ff-bg": ff_brownbg,
        "ff-pink": ff_pink,
        "ff-brownbg": ff_brownbg,
        "ff-lightbg": ff_bg,
        "ff-text": ff_text,
        "ff-links": ff_links,
        "ff-red": ff_red,
        "ffu-red": ffu_red,
        "calc-pink": calc_pink,
        offblack,
      },
    },
  },
  variants: {},
};
