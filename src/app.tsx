import { prisDraum, mndlegSparing, oppspart, updateValue } from "./state";
import type { Signal } from "@preact/signals";
import { batch } from "@preact/signals";
import { useState } from "preact/hooks";
import { UserForm } from "./bolig-form/app.tsx";

async function shareViaWebShareAPI(
  // @ts-ignore
  shareURL: string,
  // @ts-ignore
  text: string,
  years: number,
) {
  try {
    let imageURL = "/pageimgpr.png";
    if (years > 0 && years < 300) {
      imageURL = "/splash/yp" + years + ".png";
    }
    const img = await fetch(imageURL);
    const file = new File(
      [await img.blob()],
      "Nudelkalkulatoren resultat.png",
      {
        type: "image/png",
        // @ts-ignore
        text: shareURL,
      },
    );

    await navigator.share({
      files: [file],
    });
  } catch (err) {
    console.error("Error from navigator.share: ", err);
  }
}

/*
 * This is very ugly. But works.
 */
function shouldUseWebShareAPI() {
  return (
    // Share API must be available
    // @ts-ignore
    navigator.share &&
    // It must be a phone (hacky UA check, or touchscreen)
    (/(Mobi|Android|iOS|iP(hone|pad))/i.test(navigator.userAgent) ||
      navigator.maxTouchPoints > 0)
  );
}

function useExamplePerson() {
  batch(() => {
    prisDraum.value = "3 500 000";
    mndlegSparing.value = "2 000";
    oppspart.value = "20 000";
  });
}

function stringThingToNumber(thing: string) {
  return Number.parseInt(thing.replace(/\s+/g, "").replace(/,/g, ""));
}

interface NudelInputFieldProps {
  field: Signal<string>;
  label: string;
  placeholder: string;
}

function numberFormatter(numStr: string) {
  const number = stringThingToNumber(numStr);
  const formatted = new Intl.NumberFormat("no-NO", {
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
  }).format(number);
  console.log(formatted);
  return formatted;
}

function NudelInputField({ field, label, placeholder }: NudelInputFieldProps) {
  return (
    <div class="pb-3 mb-4">
      <div class="pb-1 text-xl text-left uppercase font-montserrat">
        {label}
      </div>
      <input
        type="text"
        inputmode="numeric"
        value={field.value}
        placeholder={placeholder}
        class="block w-full py-3 pl-4 pr-20 border-0 placeholder:font-montserrat placeholder:text-black rounded-md ring-1 ring-inset ring-ff-red focus:ring-2 focus:ring-inset focus:outline-none sm:text-sm sm:leading-6"
        onInput={(event) =>
          updateValue(field, numberFormatter(event.currentTarget.value))
        }
      />
    </div>
  );
}

interface UtrekningDisplayProps {
  sparingIAAr: number;
  mndVissNoverandeTempo: number;
}
function UtrekningDisplay({
  sparingIAAr,
  mndVissNoverandeTempo,
}: UtrekningDisplayProps) {
  if (sparingIAAr < 1) {
    return (
      <div class="mt-6">
        <div class="text-4xl">
          Gratulerer! Du slipper å spise nudler for å ha råd til bolig!
        </div>
      </div>
    );
  }
  let debug;
  if (import.meta.env.DEV) {
    debug = (
      <div class="text-black/25">
        (debug: tek {(mndVissNoverandeTempo / 12).toFixed(2)} år utan omlegging
        til nudler)
      </div>
    );
  }
  const shareURL = encodeURIComponent(window.location.href);
  const genericText = encodeURIComponent(
    "Jeg må spise nudler i " + sparingIAAr + " for å ha råd til drømmeboligen!",
  );
  let shareAPIVariant = null;
  if (shouldUseWebShareAPI()) {
    shareAPIVariant = (
      <>
        <a
          href="#"
          onClick={(ev) => {
            ev.preventDefault();
            shareViaWebShareAPI(shareURL, genericText, sparingIAAr);
          }}
        >
          Instagram/Snapchat
        </a>
        ,{" "}
      </>
    );
  }

  return (
    <div class="mt-6">
      <div class="text-4xl font-canela">
        Du må spise nudler i {sparingIAAr} år!
      </div>
      <div class="text-xl font-montserrat">
        Boligdrømmen er kun {sparingIAAr} år unna om du legger om kostholdet
        utelukkende til nudler. Med mindre boligprisene ikke endrer seg,
        naturligvis.
        <br />
        <br />
        Del ditt resultat på: {shareAPIVariant}
        <a
          name="fb_share"
          href={"https://www.facebook.com/sharer.php?u=" + shareURL}
        >
          Facebook
        </a>
        ,{" "}
        <a
          href={
            "http://twitter.com/share?text=" + genericText + "&url=" + shareURL
          }
        >
          Twitter/X
        </a>
        ,{" "}
        <a
          href={
            "https://mastodonshare.com/?text=" +
            genericText +
            "&url=" +
            shareURL
          }
        >
          Mastodon
        </a>
      </div>
      <div class="mt-10">
        <UserForm showShareLinks={false} />
      </div>
      {debug}
    </div>
  );
}

function Utrekning() {
  const pris = stringThingToNumber(prisDraum.value);
  const sparing = stringThingToNumber(mndlegSparing.value);
  const allereieSpart = stringThingToNumber(oppspart.value);

  if (
    Number.isNaN(pris) ||
    Number.isNaN(sparing) ||
    Number.isNaN(allereieSpart) ||
    pris < 100000 ||
    (sparing < 9 && sparing !== 0)
  ) {
    return <div />;
  }

  /*
   * 5 stk 85 gram Nudler kyllingssmak fra REMA 1000.
   * https://oda.com/no/products/15202-rema-1000-nudler-med-biffsmak-5stk-x-85g/
   *
   * Mann+kvinne 20-30 år seier SIFO har eit budsjett på 8380/mnd (henta frå
   * sak datert 09.10.2023). Delt på 2 gir eit ca. matbudsjett pr. individ på
   * 4.190 NOK/mnd
   *
   * https://www.tv2.no/nyheter/innenriks/2024-sa-mye-storre-ma-matbudsjettet-ditt-vaere/16113661/
   *
   * Utrekning av ekstraSparingVissBerreNudler:
   *
   * Nudler: 25.8*30.5=786.9
   *        (1pk/dg)
   * Spart: 4190-786.9=3403.1
   */
  const ekstraSparingVissBerreNudler = 3403.1;
  // Pris = 15% av totalsum
  const prisMinusSparing = pris * 0.15 - allereieSpart;
  const mndVissNoverandeTempo = prisMinusSparing / sparing;
  const mndVissNudler =
    prisMinusSparing / (sparing + ekstraSparingVissBerreNudler);

  const sparingIAAr = Math.round(mndVissNudler / 12);

  if (history && history.replaceState) {
    if (sparingIAAr == 0) {
      history.replaceState(null, "", "/");
    } else if (sparingIAAr < 300) {
      history.replaceState(null, "", "/r" + sparingIAAr);
    } else {
      history.replaceState(null, "", "/r300");
    }
  }

  return (
    <UtrekningDisplay
      sparingIAAr={sparingIAAr}
      mndVissNoverandeTempo={mndVissNoverandeTempo}
    />
  );
}

export function App() {
  let examplePerson;
  if (import.meta.env.DEV) {
    const [showExampleLink, setShowExampleLink] = useState(true);
    examplePerson = (
      <div class={"" + (showExampleLink ? "" : "hidden")}>
        <a
          href="#"
          onClick={() => {
            useExamplePerson();
            setShowExampleLink(false);
            return false;
          }}
        >
          Bruk eksempelperson
        </a>
      </div>
    );
  }

  return (
    <>
      <div>
        <div class="columns-1 md:columns-1">
          <div>
            <NudelInputField
              field={prisDraum}
              label="Pris på din drømmebolig"
              placeholder="Skriv prisen her"
            />
          </div>
        </div>
        <div class="columns-1 md:columns-1">
          <div>
            <NudelInputField
              field={mndlegSparing}
              label="Din månedlige sparing"
              placeholder="Skriv beløpet her"
            />
          </div>
        </div>
        <div class="columns-1 md:columns-1">
          <div>
            <NudelInputField
              field={oppspart}
              label="Dine oppsparte midler"
              placeholder=""
            />
          </div>
        </div>
      </div>

      <Utrekning />
      {examplePerson}
    </>
  );
}
