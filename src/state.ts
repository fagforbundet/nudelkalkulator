import { signal } from "@preact/signals";
import type { Signal } from "@preact/signals";

const prisDraum = signal("");
const mndlegSparing = signal("");
const oppspart = signal("0");

function updateValue(state: Signal<string>, value: string) {
  state.value = value;
}

export { prisDraum, mndlegSparing, oppspart, updateValue };
