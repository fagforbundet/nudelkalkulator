import puppeteer from "puppeteer";

async function buildMain() {
  // Launch the browser and open a new blank page
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Navigate the page to a URL
  await page.goto("https://dev.nudel.fagforbundet.no/some-bg.html");

  // Set screen size
  // 768x465+0+0
  await page.setViewport({ width: 868, height: 565 });
  for (let i = 0; i <= 300; i++) {
    process.stdout.write("\rMain: " + i);
    await page.evaluate((y) => {
      let year = y;
      if (year == 300) {
        year = "300+";
      }
      document.querySelector("#year").innerHTML = year;
    }, i);
    await page.screenshot({ path: "public/splash/y" + i + ".png" });
  }

  process.stdout.write("\rMain: base");
  await page.evaluate(() => {
    document.querySelector("#basetext").classList.remove("hidden");
    document.querySelector("#maintext").classList.add("hidden");
  });
  await page.screenshot({ path: "public/pageimg.png" });

  process.stdout.write("\rMain: done\n");
  await browser.close();
}
async function buildPortrait() {
  // Launch the browser and open a new blank page
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  // Navigate the page to a URL
  await page.goto("https://dev.nudel.fagforbundet.no/some-bg-portrait.html");

  // Set screen size
  // 768x465+0+0
  await page.setViewport({ width: 565, height: 868 });
  for (let i = 0; i <= 300; i++) {
    process.stdout.write("\rPortrait: " + i);
    await page.evaluate((y) => {
      let year = y;
      if (year == 300) {
        year = "300+";
      }
      document.querySelector("#year").innerHTML = year;
    }, i);
    await page.screenshot({ path: "public/splash/yp" + i + ".png" });
  }

  process.stdout.write("\rPortrait: base");
  await page.evaluate(() => {
    document.querySelector("#basetext").classList.remove("hidden");
    document.querySelector("#maintext").classList.add("hidden");
  });
  await page.screenshot({ path: "public/pageimgpr.png" });
  process.stdout.write("\rPortrait: done\n");

  await browser.close();
}

(async () => {
  await buildMain();
  await buildPortrait();
})();
